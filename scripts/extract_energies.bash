#!/bin/bash

rg=NeTD

for i in {00..20}
do
	files=`ls | grep out | grep $rg\_$i`
	energies=`for file in $files
	do
		cat $file | grep after | grep cm | awk '{print $5}'
	done`
	echo $energies | xargs -n 1 | sort -n | xargs #| awk '{print $1}'
done
