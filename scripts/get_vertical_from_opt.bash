#!/bin/bash

files=`cat convex.txt | awk '{print $1}'`

for file in $files
do
	echo -n $file " "
	cat $file | grep "Transition" | awk '{print $3, $4, $5}'
done
