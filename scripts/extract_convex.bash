#!/bin/bash

rg=Xe

rm -f convex.txt

for i in {00..09}
do
	files=`ls | grep out | grep $rg\_$i`
	grep "" $files | grep after | grep cm\
	   	| sed 's/\:/\ /' | awk '{print $1, $6}' | xargs -n 2 | sort -nk 2 | xargs -n 2 2>/dev/null | head -n 1 --quiet >> convex.txt
#	echo "" >> convex.txt
done
