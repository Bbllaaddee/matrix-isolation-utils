import numpy as np
import sys
import math
from scipy.interpolate import CubicSpline

def long_range_func(x: float, c6: float = 1):
    return -c6/x**6

def long_range_deriv(x:float, c6: float = 1):
    return 6*c6/x**7

def close_range_func(x:float, A: float, b: float):
    return A*np.exp(-b*x)

def close_range_deriv(x: float, A: float, b: float):
    return -b*A*np.exp(-b*x)


x,y = numpy.loadtxt(sys.argv[1], unpack=True)
x /= 0.529177
y /= 219474.6

c6_start = (y[-1] - y[-2]) * (x[-1]**6 * x[-2]**6) / (x[-1]**6 - x[-2]**6)
u0 = y[-1] - long_range_func(x[-1], c6_start)
y -= u0

b_start = np.log(y[0] - y[1]) / (x[1] - x[0])
A_start = y[0] / close_range_func(x[0], b_start)

splines = CubicSpline(x, y,
        bc_type=((1, close_range_deriv(x[1], A_start, b_start)),
                 (1,  long_range_deriv(x[-2], c6_start))))

for r in np.arange(5, 25.05, 0.1):
    c6_curr = -spline(r) * r**6
    s = 0.0
    for k in np.arange(r, x[-2]+0.005, 0.01):
        s += (-c6_curr/k**6 - spline(k)) * (-c6_curr/k**6 - spline(k))

n_splines = int(sys.argv[2])
x_left = float(sys.argv[3])
x_right = float(sys.argv[4])

x_new = np.linspace(x_left, x_right, n_splines+1)
y_new = splines(x_new)

splines_final = CubicSpline(x_new, y_new,
        bc_type=( (1, splines(x_left,1)), (1, splines(x_right,1)) ))


