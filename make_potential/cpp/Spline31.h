#pragma once

class Spline31
{
private:
	double boundary_diff_left;
	double boundary_diff_right;

	std::vector<double> x;
	std::vector<double> y;
	std::vector<double> c;

public:
	Spline31() = default;
	Spline31(double l, double r, std::vector<double> const& x, std::vector<double> const& y)
		: boundary_diff_left(l), boundary_diff_right(r), x(x), y(y), c((x.size()-1)*4)

	int solve();	

	int getCoeff(int, double&, double&,  double&, double&, double&, double&);

	double getY(double, int&);
	double getDY(double, int&);

};

