#!/bin/bash

REPO_PATH="/home/boris/repos/ds/matrix-isolation/bin"

nSplines=32             # Number of splines for middle-range
CloseRangeCutOff=150    # Lowest energy for exponential fit
LongRangeAccuracy=0.05  # Integral error over dispersion term

#################################
#         VARIABLES END         #
#################################

CM_TO_BOHR=219474.6
HARTREE_TO_A=0.529

function makePotentialAuto {
	fileInp=$1
	nSpline=$2
	Emax=$3
	Emin=$4
	Emax=`LC_ALL=C echo "$Emax / 219474.6 " | bc -l`

	fileOut=`echo $fileInp | sed "s/txt$/sr$nSpline.dat/"`

	xEnd=`makePotential $fileInp\
		| sed -n '/Поиск нового/,/Сравнение/p'\ # Print from "Поиск нового" to "Сравнение"
		| sed '1,2d' | sed '$ d'\				# remove first two and one last line
		| awk '{if ($1==int($1)) print $1,$2}'\ # Print only lines that start with number
		| awk -v Elim="$Emin" '{if ( $2 < Elim ) print $1}'\ # Print first row when second is smaller than c
		| head -n 1` # print first line

	xStart=`makePotential $fileInp\
		| sed -n '/Исходные данные/,/Исходный сплайн/p'\ # Print From To
		| sed '1,2d' | sed '$ d'\
		| awk -v Elim="$Emax" '{if ( $2 > Elim ) print $1}'\
		| tail -n 1\
		| awk '{print int($1)}'`

	makePotential $fileInp $nSpline $xStart $xEnd | sed -n '/-------/,$p' | sed '1d' > $fileOut
	echo $fileInp $fileOut $nSpline $Emax $Emin $xStart $xEnd
}

export PATH="$PATH:$REPO_PATH"

makePotentialAuto $1 $nSplines $CloseRangeCutOff $LongRangeAccuracy


exit 0

