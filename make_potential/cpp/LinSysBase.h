#ifndef LINSYSBASE_H
#define LINSYSBASE_H

#include "DGESV_DEF.h"

class LinSysBase
// solves linear system
{
private:
	std::vector<double> a;
	std::vector<double> b;
	std::vector<double> x;

public:
	LinSysBase() = default;
	LinSysBase(int dim, std::vector<double> const& a, std::vector<double> const& b) : a(a), b(b), x(b.size()) {}

	void solve()
	{
		Diag::solve(a,b);
		if (info!=0) { throw std::runtime_error("Can't solve linear system!"); }
	}

	std::vector<double> get_solution() { return x; }
	double get_xi(int) { return x[i]; }

};

#endif
