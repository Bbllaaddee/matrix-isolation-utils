#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include "Spline31.h"

template<typename T>
inline T pow6(T a) noexcept { return a*a*a*a*a*a; }

void read_file(std::string_view filename, std::vector<double>& x, std::vector<double>& y)
{
	std::ifstream inp(filename);
	double tmp1;
	double tmp2;
	while(inp >> tmp1 >> tmp2)
	{
		x.push_back(tmp1/0.529177);
		y.push_back(tmp2/219474.6);
	}
}

int main(int argc, char* argv[])
{
	int n_splines = 32; // число сплайнов
	double x10 = 5.0;   // r1
	double x1n = 17.0;  // r2
	if ((argc!=5) && (argc!=2))
	{
		std::cout << "!!! ./program.exe filename.txt(A,cm-1) Nspile xStart xEnd\n";
		return 1;
	}

	if (argc==5){
		n_splines=atoi(argv[2]);
		x10=atof(argv[3]);
		x1n=atof(argv[4]);
	}

	std::vector<double> x;
	std::vector<double> y;
	read_file(argv[1], x, y);
	size_t const n = x.size();

//	определить параметры краевых функций по двум точкам
	double const c6 = (y[n-1] - y[n-2]) * pow6(x[n-1])*pow6(x[n-2]) / (pow6(x[n-1]) - pow6(x[n-2]));
	double u0 = y[n-1] + c6/pow6(x[n-1]);
	for(int i=0; i<n; i++) y[i] -= u0;

	double a = log(y[0]/y[1])/(x[1]-x[0]);
	double A = y[0]/exp(-a*x[0]);
	std::cout << "Исходные краевые функции (U0-c6/r^6 - справа; Aexp(-ar) - слева)\n"
	std::cout << "С6       U0      A     a (все в au)\n";
	std::cout << c6 << ' ' << u0 << ' ' << A << ' ' << a << '\n';
	std::cout<<"Исходные данные, поправленные на u0 (au)\n"
	std::cout << "x        U\n";
	for (int i=0; i<n; i++) { std::cout << std::setprecision(10) << x[i] << ' ' << y[i] << '\n'; }

//	ГЕНЕРАЦИЯ ИСХОДНОГО СПЛАЙНА			
	const int N = n-1;
	double* v = new double [2*n];
	for(int i=0; i<N; ++i){
		v[2*i]   = x[i+1];
		v[2*i+1] = y[i+1];
	};
	Spline31 spline31;
	spline31.init(N, v, -a*A*exp(-a*x[1]), 6.0*c6/(pow(v[2*N-2],7)));
	spline31.solve();
	int info;
	std::cout<<"Исходный сплайн\n";
	for (double q=v[0]; q<=v[2*N-2]; q+=0.1) std::cout << std::setprecision(10) << q << ' ' << spline31.getY(q,info) << '\n';

//	Поиск нового c6
 	std::cout<<"Поиск нового c6\n"
	std::cout << "Rx    Int((Ux-Uspline)^2),cm-1    C6(Rx)\n";
	for (double rn=5; rn<=25; rn+=0.1) {
		double c6n = -spline31.getY(rn,info)*pow6(rn);
		double s = 0;
		for (double k=rn; k<v[2*N-2]; k+=0.01) {
			double k6 = pow6(k);
			s += (-c6n/k6 - spline31.getY(k,info))*(-c6n/k6 - spline31.getY(k,info));
		}
		std::cout << rn << "        " << sqrt(s*0.01)*219474.6 << "      " << c6n << '\n';
	}
	delete [] v;

//	Конечный сплайн
	double* v1 = new double [2*(n_splines+1)]; // new points after first interpolation
	for(int i=0; i<n_splines+1; ++i) {
		v1[2*i]   = x10 + (x1n-x10)*(double)i/(double)n_splines;
		v1[2*i+1] = spline31.getY(v1[2*i],info);
	}
	Spline31 spline31final;
	spline31final.init(n_splines+1, v1, spline31.getDY(x10,info),spline31.getDY(x1n,info));
	spline31final.solve();
	delete [] v1;

//	Сравнение конечного сплайна и исходного сплайна
	std::cout<<"Сравнение построенной экстраполяции с исходной\n";
	std::cout << "R     исходный сплайн   конечный сплайн\n";
	for(double r=x10; r<=x1n; r+=0.1)
		std::cout << std::setw(5) << r << ' ' << std::setw(15) << std::setprecision(10) << spline31.getY(r,info) << ' ' << spline31final.getY(r,info) << '\n';

//	Коэффициенты конечного сплайна
	std::cout << "Коэффициенты конечного сплайна ai*x'^3 + bi*x'^2 + ci*x' + di (x'=x-x(лев.граница i-го интервала))\n";
	std::cout << "i x(лев,i)  x(прав,i)  ai     bi     ci    di\n";
	double ai, bi, ci, di, xmin, xmax;
	for(int i=0; i<n_splines; i++){
		spline31final.getCoeff(i+1,xmin,xmax,ai,bi,ci,di);
		std::cout << i+1 << ' ' << std::setprecision(10)
			      << xmin << ' ' << xmax << ' ' << ai << ' ' << bi << ' ' << ci << ' ' << di << '\n';
	}

//  Коэффициенты краевых функций (c6/x^6);  Aexp(-ax)
	std::cout << "Конечные значения c6, A и a\n";
	std::cout << "c6=" << -spline31.getY(x1n,info)*pow6(x1n) << '\n';
	double a1 = -spline31.getDY(x10,info)/spline31.getY(x10,info);
	std::cout << "a=" << a1 << '\n';
	std::cout << "A=" << spline31.getY(x10,info)/exp(-a1*x10) << '\n';
	std::cout << "----------------------------------\n";
	std::cout << -spline31.getY(x1n,info)*pow6(x1n) << '\n' << a1 << '\n' << spline31.getY(x10,info)/exp(-a1*x10) << '\n';
	std::cout << x10 << ' ' << x1n << ' ' << n_splines << '\n';
	for(int i=0; i<n_splines; i++){
		spline31final.getCoeff(i+1,xmin,xmax,ai,bi,ci,di);
		std::cout << std::setprecision(10) << ai << ' ' << bi << ' ' << ci << ' ' << di << '\n';
	}
	return 0;				
}
