#include "Spline31.h"
#include "LinSysBase.h"

int Spline31::solve(){
	if (!initialize) return -1;
// В принципе система редуцируется до N-1 уравнений,
// но на моих задачах размерность 3(N-1) не критична.
	LinSysBase linsys;
	const int size = 3*(dim-1);
	double* a   = new double [size*size];
	double* b   = new double [size];
	double* res = new double [size];
	for(int i=0; i<size; i++) b[i] = 0;
	for(int i=0; i<size*size; i++) a[i] = 0;
	double* dx = new double [dim-1];
	for (int i=0; i<dim-1; i++) dx[i] = x[i+1] - x[i];
	for(int i=0; i<dim-1; i++) {
		a[i*(size+3)]   = dx[i]*dx[i]*dx[i];
		a[i*(size+3)+1] = dx[i]*dx[i];
		a[i*(size+3)+2] = dx[i];
		b[i] = y[i+1] - y[i];
	}
	for(int i=0;i<dim-2;i++) {
		a[size*(i+dim-1)+i*3]   = 3*dx[i]*dx[i];
		a[size*(i+dim-1)+i*3+1] = 2*dx[i];
		a[size*(i+dim-1)+i*3+2] = 1.;
		a[size*(i+dim-1)+i*3+5] = -1.;
		a[size*(i+2*(dim-1))+i*3]   = 3*dx[i];
		a[size*(i+2*(dim-1))+i*3+1] = 1.;
		a[size*(i+2*(dim-1))+i*3+4] = -1.;
	}
	a[size*(2*(dim-1)-1)+2] = 1.;
	b[(2*(dim-1)-1)] = dy0;
	a[9*(dim-1)*(dim-1)-3] = 3*dx[dim-2]*dx[dim-2];
	a[9*(dim-1)*(dim-1)-2] = 2*dx[dim-2];
	a[9*(dim-1)*(dim-1)-1] = 1.;
	b[(size-1)]=dyN;
	
	linsys.init(size, a, b);
	int info = linsys.solve();
	linsys.getSolution(res);
	for (int i=1; i<dim; i++) {
		c[i*4-4] = res[i*3-3];
		c[i*4-3] = res[i*3-2];
		c[i*4-2] = res[i*3-1];
		c[i*4-1] = y[i-1];
	}
	solved = !(bool)info;
	delete [] dx;
	delete [] a;
	delete [] b;
	delete [] res;
	return info;
}

int Spline31::getCoeff(int interval,
			double& x_l, double& x_r,
			double& c3 , double& c2, double& c1, double& c0){
	x_l = x[interval-1];
	x_r = x[interval];
	c3 = c[interval*4-4];
	c2 = c[interval*4-3];
	c1 = c[interval*4-2];
	c0 = c[interval*4-1];
	return 0;
}
